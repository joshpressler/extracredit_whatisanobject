
/*
 * What is an object?
 * 
 * An object in software can be compared to any object in the real world.  Both objects have a state (name, color, etc.) and a behavior (running, 
 * sleeping, etc.).  There are three steps in utilizing objects in java.  
 * 
 * 1. Declaration:  variable declaration with a name and type
 * 2. Instantiation: using the key word 'new'
 * 3. Initialization: following 'new', there is a call to a constructor
 */

public class ExampleObject {
	
	public ExampleObject(String item){
		System.out.println("This object is a/an: " + item);				//prints what the item is 
	}

}
